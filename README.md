# com-wui-framework v2.3.0

> Developer tools for creation of graphical user interface based on web technologies

![WUI Logo](Logo.png)

## Project Documentation
This repository provides only link to documentation. 

**For more details see [WUI Framework Docs](https://docs.wuiframework.com).**

## History

### v2.3.0
Migration to Confluence.
### v2.0.0
Namespaces refactoring.
### v1.0.0
Initial release

## License

This software is owned or controlled by NXP Semiconductors. 
Use of this software is governed by the BSD-3-Clause License distributed with this material.
  
See the `LICENSE.txt` file distributed for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2018 [NXP](http://nxp.com/)
